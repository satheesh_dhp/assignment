package example.com.mysoftvisionassignment.pojorequest;

import java.util.List;

/**
 * Created by satheesh on 6/1/2017.
 */


public class UserRequestPojo extends  RequestObject {

    String name;
    String gender;
    String phoneNumber;
    String emailId;
    String jobType;
    String address;
    String latitude;
    String longitude;
    List<String> educaitonalQualification;

    public String getPhotoBitmap() {
        return photoBitmap;
    }

    public void setPhotoBitmap(String photoBitmap) {
        this.photoBitmap = photoBitmap;
    }

    String photoBitmap ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getEducaitonalQualification() {
        return educaitonalQualification;
    }

    public void setEducaitonalQualification(List<String> educaitonalQualification) {
        this.educaitonalQualification = educaitonalQualification;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


}
