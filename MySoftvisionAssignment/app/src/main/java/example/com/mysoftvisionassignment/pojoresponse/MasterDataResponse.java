package example.com.mysoftvisionassignment.pojoresponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by satheesh on 6/1/2017.
 */

public class MasterDataResponse  {


    public MasterDataResponse.MasterData getMasterData() {
        return MasterData;
    }

    public void setMasterData(MasterDataResponse.MasterData masterData) {
        MasterData = masterData;
    }

    private MasterData MasterData = new MasterData();

    public class MasterData
    {
        public List<ClassQualification> getEducationalQualification() {
            return EducationalQualification;
        }

        public void setEducationalQualification(List<ClassQualification> educationalQualification) {
            EducationalQualification = educationalQualification;
        }

        public List<ClassJobTypes> getJobTypes() {
            return JobTypes;
        }

        public void setJobTypes(List<ClassJobTypes> jobTypes) {
            JobTypes = jobTypes;
        }

        List<ClassQualification>EducationalQualification = new ArrayList<>();
       List<ClassJobTypes>JobTypes = new ArrayList<>();



        public class ClassQualification
        {
            public String getQualification() {
                return Qualification;
            }

            public void setQualification(String qualification) {
                Qualification = qualification;
            }

            String Qualification;
        }

        public class ClassJobTypes
        {
            public String getJobType() {
                return JobType;
            }

            public void setJobType(String jobType) {
                JobType = jobType;
            }

            String JobType;
        }
    }
}
