package example.com.mysoftvisionassignment.utility;

/**
 * Created by satheesh on 6/1/2017.
 */

public class GlobalConstants {

    public static final String  USER_MASTER_DATA_URL ="https://evaluation-exercise.herokuapp.com/getMasterData";
    public static final String  SAVE_USER_DATA_URL ="https://evaluation-exercise.herokuapp.com/profile";
    public static final int FETCH_MASTER_DATA = 0x0001;
    public static final int SAVE_USER_DATA = 0x0002;



}
