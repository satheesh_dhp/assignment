package example.com.mysoftvisionassignment;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import example.com.mysoftvisionassignment.model.NameBean;
import example.com.mysoftvisionassignment.pojorequest.UserRequestPojo;
import example.com.mysoftvisionassignment.pojoresponse.MasterDataResponse;
import example.com.mysoftvisionassignment.utility.GlobalConstants;
import example.com.mysoftvisionassignment.webservices.ServiceCall;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "DATA";
    private EditText firstNameEditText, lastNameEditText, genderEditText, phoneNoEditText,
            emailEditText, jobTypeEditText, addressEditText;
    private ImageView usersPhotoImageView;
    private Button saveButton,clickButton,addressButton;
    private AutoCompleteTextView autoCompleteJobType;
    private int mCurrentRequestType;
    private UserController userController;
    private static final int GET_FROM_GALLERY = 1;
    private Bitmap mBitmapToSave;
    private static final int REQUEST_CODE_CAPTURE_IMAGE = 2;
    private ListView listView;
    private List<NameBean> qualificationList;
    private List<String>selectedQualification;
    private MultiSelectAdapter objAdapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        userController = new UserController();
        userController.getUserData();
    }

    private void initView() {
        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        genderEditText = (EditText) findViewById(R.id.genderEditText);
        phoneNoEditText = (EditText) findViewById(R.id.phoneNoEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        usersPhotoImageView = (ImageView) findViewById(R.id.usersPhotoImageView);
        listView = (ListView) findViewById(R.id.listview);

        autoCompleteJobType = (AutoCompleteTextView) findViewById(R.id.autoCompletejobType);
        saveButton = (Button) findViewById(R.id.saveButton);
        clickButton = (Button) findViewById(R.id.clickButton);
        addressButton = (Button) findViewById(R.id.addressButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validateData())
                userController.saveUserData();
                else
                    Toast.makeText(MainActivity.this,"Please enter all the data!",Toast.LENGTH_SHORT).show();
            }
        });

        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(null)
                        .setTitle("")
                        .setMessage("Please select the option")
                        .setPositiveButton("GALLERY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
                            }

                        })
                        .setNegativeButton("CAMERA", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mBitmapToSave == null) {
                                    startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                                            REQUEST_CODE_CAPTURE_IMAGE);

                                }
                            }

                        })
                        .show();
            }
        });
        addressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    private class UserController implements Response.Listener<JSONObject>, Response.ErrorListener {

        private void getUserData() {
            UserRequestPojo masterData = new UserRequestPojo();
            mCurrentRequestType = GlobalConstants.FETCH_MASTER_DATA;
            try {
                ServiceCall.getInstance().makeRequest(GlobalConstants.FETCH_MASTER_DATA, masterData, this, this, Request.Method.GET);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void saveUserData()
        {
            UserRequestPojo userRequestPojo = new UserRequestPojo();
            userRequestPojo.setName(firstNameEditText.getText().toString()+" "+lastNameEditText.getText().toString());
            userRequestPojo.setGender(genderEditText.getText().toString());
            userRequestPojo.setPhoneNumber(phoneNoEditText.getText().toString());
            userRequestPojo.setEmailId(emailEditText.getText().toString());
            userRequestPojo.setJobType(autoCompleteJobType.getText().toString());
            userRequestPojo.setAddress(addressEditText.getText().toString());
            userRequestPojo.setPhotoBitmap(convertBitMapToBase64());
            userRequestPojo.setEducaitonalQualification(selectedQualification);
            userRequestPojo.setLatitude("688.00");
            userRequestPojo.setLongitude("9877.00");
            mCurrentRequestType = GlobalConstants.SAVE_USER_DATA;
            try {
                ServiceCall.getInstance().makeRequest(GlobalConstants.SAVE_USER_DATA, userRequestPojo, this, this, Request.Method.POST);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            switch (mCurrentRequestType) {
                case GlobalConstants.FETCH_MASTER_DATA:
                    Log.e(TAG,volleyError.toString());
                    break;
                case GlobalConstants.SAVE_USER_DATA:
                    break;
            }
        }

        @Override
        public void onResponse(JSONObject jsonObject) {
            switch (mCurrentRequestType) {
                case GlobalConstants.FETCH_MASTER_DATA:

                    try {

                         Gson gson = new GsonBuilder().create();
                        MasterDataResponse masterDataResponse = new MasterDataResponse();
                        masterDataResponse = gson.fromJson(jsonObject.toString(),MasterDataResponse.class);
                        List<String> jobTypeList = new ArrayList<>();
                        qualificationList = new ArrayList<>();
                        NameBean nameBean=null;

                         for (MasterDataResponse.MasterData.ClassJobTypes obj : masterDataResponse.getMasterData().getJobTypes())
                        {
                            jobTypeList.add(obj.getJobType());
                        }

                       ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                (MainActivity.this,android.R.layout.simple_list_item_1,jobTypeList);
                        autoCompleteJobType.setAdapter(adapter);

                        for (MasterDataResponse.MasterData.ClassQualification obj : masterDataResponse.getMasterData().getEducationalQualification())
                        {
                            nameBean = new NameBean();
                            nameBean.setName(obj.getQualification());
                            qualificationList.add(nameBean);
                        }
                        setAdapterToListview();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case GlobalConstants.SAVE_USER_DATA:
                    summaryShow(jsonObject);
                    Log.e(TAG,jsonObject.toString());
                    break;
            }
        }

}

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_CAPTURE_IMAGE:
            case GET_FROM_GALLERY:

                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == GET_FROM_GALLERY) {
                        Uri selectedImage = data.getData();
                        try {
                            mBitmapToSave = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                            usersPhotoImageView.setImageBitmap(mBitmapToSave);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else if (requestCode == REQUEST_CODE_CAPTURE_IMAGE) {
                        mBitmapToSave = (Bitmap) data.getExtras().get("data");
                        usersPhotoImageView.setImageBitmap(mBitmapToSave);
                    }
                }
                break;
        }
    }

    // setAdapter Here....

    public void setAdapterToListview() {

        // Sort Data Alphabatical order
        Collections.sort(qualificationList, new Comparator<NameBean>() {

            @Override
            public int compare(NameBean lhs, NameBean rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        selectedQualification = new ArrayList<>();
        objAdapter = new MultiSelectAdapter(MainActivity.this, qualificationList,selectedQualification);
        listView.setAdapter(objAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                CheckBox chk = (CheckBox) view.findViewById(R.id.checkbox);
                NameBean bean = qualificationList.get(position);
                if (bean.isSelected()) {
                    bean.setSelected(false);
                    chk.setChecked(false);
                } else {
                    bean.setSelected(true);
                    chk.setChecked(true);
                }

            }
        });

    }

    private String convertBitMapToBase64() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        mBitmapToSave.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void summaryShow(JSONObject jsonObject)
    {
        // Create custom dialog object
        final Dialog dialog = new Dialog(MainActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.customdialog);
        // Set dialog title
        dialog.setTitle("Summary Page");

        // set values for custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.textDialog);

        try {
            String returnData = "Name : "+jsonObject.getString("name")+"\n";
            returnData = returnData+ "Gender: " + jsonObject.getString("gender")+"\n" +"Phone No:"+jsonObject.getString("phoneNumber")+"\n" +"Email id :"+
                    jsonObject.getString("emailId")+"\n" + "Job Type : "+
                    jsonObject.getString("jobType")+"\n" +"Address : " +jsonObject.getString("address")+"\n" +"Qualifications : " +jsonObject.getString("educaitonalQualification") ;
            text.setText(returnData);
        }
        catch (JSONException ex)
        {}
        dialog.show();

        Button closeButton = (Button) dialog.findViewById(R.id.closeButton);
        // if closeButton is clicked, close the custom dialog
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });


    }

    private boolean validateData()
    {
        boolean isValid = true;
        if(isEmpty(firstNameEditText.getText().toString())  )
            return false;
        else if(isEmpty(lastNameEditText.getText().toString()))
        return false;
        else if(isEmpty(genderEditText.getText().toString()))
            return false;
        else if(isEmpty(phoneNoEditText.getText().toString()))
        return false;
        else if(isEmpty(emailEditText.getText().toString()))
        return false;
        else if(isEmpty(autoCompleteJobType.getText().toString()))
            return false;
        else if(mBitmapToSave==null)
            return false;
        else if(isEmpty(addressEditText.getText().toString()))
            return false;
        else if(selectedQualification.size()==0)
            return false;

        return  isValid;
    }
    public static boolean isEmpty(CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }

}
