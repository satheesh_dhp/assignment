package example.com.mysoftvisionassignment.webservices;

import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import example.com.mysoftvisionassignment.pojorequest.RequestObject;

/**
 * Created by satheesh on 6/1/2017.
 */

public interface IServiceCall {
    void makeRequest(int requestType, RequestObject requestData, Response.Listener<JSONObject> response, Response.ErrorListener errorListener,int Verb) throws JSONException;

    String getRequestUrl(final int requestType);
}
