package example.com.mysoftvisionassignment.webservices;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import example.com.mysoftvisionassignment.AppController;
import example.com.mysoftvisionassignment.pojorequest.RequestObject;
import example.com.mysoftvisionassignment.utility.GlobalConstants;

/**
 * Created by satheesh on 6/1/2017.
 */

public class ServiceCall implements  IServiceCall {
    private static final RetryPolicy mRetryPolicy = new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 60, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    private static IServiceCall sServiceCall;
    private Response.Listener<JSONObject> mResponseListener;
    private Response.ErrorListener mErrorListener;


    private ServiceCall() {
        sServiceCall = this;
    }

    public static IServiceCall getInstance() {
        if (sServiceCall == null) {
            sServiceCall = new ServiceCall();
        }
        return sServiceCall;
    }
    @Override
    public void makeRequest(int requestType, RequestObject requestData, Response.Listener<JSONObject> response,
                            Response.ErrorListener errorListener,int verb) throws JSONException {

            String jsonStr = "";
            mResponseListener = response;
            mErrorListener = errorListener;
            String url = getRequestUrl(requestType);

            Gson gson = new Gson();
            jsonStr = gson.toJson(requestData);

            JSONObject jsonObject = new JSONObject(jsonStr);
            JsonObjectRequest request = new JsonObjectRequest(verb, url, jsonObject,
                    mResponseListener, mErrorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            request.setRetryPolicy(mRetryPolicy);
            addRequestToQueue(request, getTag(requestType));

            Log.e(getTag(requestType), " URL:" + getRequestUrl(requestType) + jsonStr);

    }

    @Override
    public String getRequestUrl(int requestType) {
        String requestUrl = "";
        switch (requestType) {
            case GlobalConstants.FETCH_MASTER_DATA:
                requestUrl = GlobalConstants.USER_MASTER_DATA_URL;
                break;
            case GlobalConstants.SAVE_USER_DATA:
                requestUrl = GlobalConstants.SAVE_USER_DATA_URL;
                break;
        }
            return requestUrl;

    }

    private void addRequestToQueue(Request request, String tag) {
           AppController.getInstance().addToRequestQueue(request, tag);
    }

    private String getTag(int requestType) {
        String tag = "";
        switch (requestType) {
            case GlobalConstants.FETCH_MASTER_DATA:
                tag = "FETCH_MASTER_DATA";
                break;
            case GlobalConstants.SAVE_USER_DATA:
                tag = "SAVE_USER_DATA";
                break;
        }
        return  tag;
    }
}
