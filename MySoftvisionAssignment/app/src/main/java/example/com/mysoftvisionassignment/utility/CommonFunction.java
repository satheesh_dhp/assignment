package example.com.mysoftvisionassignment.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import example.com.mysoftvisionassignment.AppController;

/**
 * Created by satheesh on 6/1/2017.
 */

public class CommonFunction {
    public static boolean checkInternetConnection() {
        NetworkInfo networkInfo = ((ConnectivityManager) AppController.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return (networkInfo != null) && (networkInfo.isConnected());
    }
}
